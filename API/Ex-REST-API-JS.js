
// Criar Usuário
async function createUser() {
  const response = await fetch(`https://api.clarizen.com/V2.0/services/data/CreateAndRetrieve`, {
    method: 'POST', // Tipo da chamada para o servidor. 
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers : {
        Authorization: `ApiKey ${process.env.API_KEY_DEV}` // chave de API para evitar de fazer uma chamada apenas para sessão. em caso de sessão utilizar `Session` ao invez de ApiKey
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify({
        entity: {
          Id: "/User",
          DisplayName: "User Clarizen",
          Email: "user@user.com"
        }
    }) // corpo do que será enviado para o servidor.
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

// Listar Multiplos Usuário
async function retrieveUsers() {
  const response = await fetch(`https://api.clarizen.com/V2.0/services/data/entityQuery`, {
    method: 'POST', // Tipo da chamada para o servidor. 
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers : {
        Authorization: `ApiKey ${process.env.API_KEY_DEV}` // chave de API para evitar de fazer uma chamada apenas para sessão. em caso de sessão utilizar `Session` ao invez de ApiKey
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify({
        typeName: "User",
        fields: ["SYSID", "DisplayName", "Email"], // campos do objeto que será listado
        // where: {
        //   and: [
        //     {
        //       leftExpression:{ "fieldName": "DisplayName"},
        //       operator: "Equal",
        //       rightExpression: { "value": "User Clarizen" }
        //     }
        //   ]
        // }  o resultado pode ser filtrado.
      }) // corpo do que será enviado para o servidor.
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

// Listar unico usuario
async function retrieveUser() {
  const response = await fetch(`https://api.clarizen.com/V2.0/services/data/objects/User/123456?fields=DisplayName,SYSID`, {
    method: 'GET', // Tipo da chamada para o servidor. 
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers : {
        Authorization: `ApiKey ${process.env.API_KEY_DEV}` // chave de API para evitar de fazer uma chamada apenas para sessão. em caso de sessão utilizar `Session` ao invez de ApiKey
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

// Atualizar Usuário
async function updateUser() {
  // A chamada upsert pode ser usada para criar e atualizar um objeto. caso o `insertEntity` fique vazio ocorrerá apenas um update e apenas uma criação caso o `updateEntity` fique vazio.
  const response = await fetch(`https://api.clarizen.com/V2.0/services/data/upsert`, {
    method: 'POST', // Tipo da chamada para o servidor. 
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers : {
        Authorization: `ApiKey ${process.env.API_KEY_DEV}` // chave de API para evitar de fazer uma chamada apenas para sessão. em caso de sessão utilizar `Session` ao invez de ApiKey
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify({
      insertEntity: {
      }, 
      updateEntity: {
        Id: "/User/123456",        
        DisplayName: "Nome Atualizado"        
      }  
    }) // corpo do que será enviado para o servidor.
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

// Deletar Usuário
async function deleteUser() {
    const response = await fetch(`https://api.clarizen.com/V2.0/services/data/objects`, {
    method: 'DELETE', // Tipo da chamada para o servidor. 
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers : {
        Authorization: `ApiKey ${process.env.API_KEY_DEV}` // chave de API para evitar de fazer uma chamada apenas para sessão. em caso de sessão utilizar `Session` ao invez de ApiKey
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify({
      Id: "/User/123456"
    }) // corpo do que será enviado para o servidor.
  });
  return response.json(); // parses JSON response into native JavaScript objects
}
